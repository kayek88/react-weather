import React, {Component} from "react";
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {fetchCurrentWeather} from '../../actions';
import {fetchAirCondition} from '../../actions';
import types from '../../actions';
import './Current.scss';

class Current extends Component {
  setCurrenPosition() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        return [
          this.props.fetchCurrentWeather(position),
          this.props.fetchAirCondition(position)
        ]
      });
    } else {
      alert(types.ALERT);
    }
  }

  componentDidMount() {
    this.setCurrenPosition();
  }

  render() {
    const { 
      currentWeather,
      airCondition 
    } = this.props;

    return (
      <section className="sec">
        <h2 className="sr-only">Pogoda na dziś</h2>

        {currentWeather.map((item, index) => (
          <h3 className="sec__title" key={index}>{item.city_name}</h3>
        ))}
          
        <div className="cards">
          {currentWeather.map((item, index) => (
            <div className="card" key={index}>
              <div className="img-wrapper">
                <img src={`https://www.weatherbit.io/static/img/icons/${item.weather.icon}.png`} alt={item.weather.description} />
              </div>

              <div class="temp txt-fw-average">
                {item.temp} <sup>o</sup>C
              </div>

              <div class="info">
                <p>{item.weather.description}</p>
                <p>Wiatr: {item.wind_spd} m/s</p>
                <p>Kierunek wiatru: {item.wind_cdir_full}</p>
              </div>
            </div>
          ))}

          {airCondition.map((item) => (
            <div className="card card--expanded" key={item.lat + item.lon}>
              <table className="table-air-conditions">
                <tbody>
                  <tr>
                    <td>Indeks jakości powietrza (AQI)</td>
                    <td className="txt-h6 txt-fw-bold">{item.aqi}</td>
                  </tr>

                  <tr>
                    <td>Stężenie powierzchniowe O3 (µg/m³)</td>
                    <td className="txt-h6 txt-fw-bold">{item.o3}</td>
                  </tr>

                  <tr>
                    <td>Stężenie powierzchniowe SO2 (µg/m³)</td>
                    <td className="txt-h6 txt-fw-bold">{item.so2}</td>
                  </tr>

                  <tr>
                    <td>Stężenie powierzchniowe NO2 (µg/m³)</td>
                    <td className="txt-h6 txt-fw-bold">{item.no2}</td>
                  </tr>

                  <tr>
                    <td>Stężenie powierzchniowe tlenku węgla (µg/m³)</td>
                    <td className="txt-h6 txt-fw-bold">{item.co}</td>
                  </tr>

                  <tr>
                    <td>Stężenie powierzchniowe cząstek stałych pm25 (µg/m³)</td>
                    <td className="txt-h6 txt-fw-bold">{item.pm25}</td>
                  </tr>

                  <tr>
                    <td>Stężenie powierzchniowe cząstek stałych pm10 (µg/m³)</td>
                    <td className="txt-h6 txt-fw-bold">{item.pm10}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          ))}
        </div>
      </section>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    currentWeather: state.currentWeather,
    airCondition: state.airCondition
  };
}

const  mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    fetchCurrentWeather, 
    fetchAirCondition
  }, dispatch)
}

const CurrentContainer = connect(mapStateToProps, mapDispatchToProps)(Current);
export default CurrentContainer;

