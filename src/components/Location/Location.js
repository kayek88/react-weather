import React, {Component} from "react";
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {
  fetchLocationWeather, 
  resetLocationWeather
} from '../../actions';
import './Location.scss';


class Location extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      input: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.submitSearch = this.submitSearch.bind(this);
  }

  handleChange = (event) => {
    this.setState({
      input: event.target.value
    });
  }

  handleKeyPress = (event) => {
    if(event.charCode === 13){  
      this.submitSearch();
    } 
  }

  submitSearch = () => {
    this.props.fetchLocationWeather(this.state.input);
    this.setState({
      input: ''
    });
  };

  resetResults = () => {
    this.props.resetLocationWeather();
  }

  render () {
    const {weather} = this.props;

    return (
      <section className="sec">
        <div className="column column--left">
          <h2 className="sr-only">Szukaj pogody</h2>
    
          <div className="searcher">
            <input className="searcher__input" type="text" placeholder="lokalizacja" value={this.state.input} onChange={this.handleChange} onKeyPress={this.handleKeyPress} />
            
            <button className="button-confirm" onClick={this.submitSearch}>
              Sprawdź pogodę
            </button>
          </div>

          <button className="button-clear" onClick={this.resetResults}>
            wyczyść wyniki
          </button>
        </div>

        <div className="column column--right">
          <div className="cards">
            {weather.map((item, index) => (
              <div className="card" key={index}>
                <div className="img-wrapper">
                  <img src={`https://www.weatherbit.io/static/img/icons/${item.weather.icon}.png`} alt={item.weather.description} />
                </div>

                <div class="temp txt-fw-average">
                  {item.temp} <sup>o</sup>C
                </div>

                <div class="info">
                  <h3 class="txt-h4 txt-fw-average">{item.city_name}</h3>
                  <p>{item.weather.description}</p>
                  <p>Wiatr: {item.wind_spd} m/s</p>
                  <p>Kierunek wiatru: {item.wind_cdir_full}</p>
                </div>
              </div>
            ))}
          </div>
        </div>
      </section>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    weather: state.weather
  };
}

const  mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    fetchLocationWeather,
    resetLocationWeather
  }, dispatch)
}

const LocationContainer = connect(mapStateToProps, mapDispatchToProps)(Location);
export default LocationContainer;
