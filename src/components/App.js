import React, {Component} from "react";
import {
  HashRouter,
  Switch,
  Route,
  NavLink
} from "react-router-dom";
import LocationContainer from './Location/Location';
import CurrentContainer from './Current/Current';
import LongtermContainer from './Longterm/Longterm';
import './App.scss';

class App extends Component {
  render() {
    return (
      <div className="container">
        <HashRouter>
          <h1 className="sr-only">Sprawdź pogodę!</h1>

          <nav className="nav">
            <h2 className="sr-only">Nawigacja</h2>
            
            <ul className="nav__list">
              <li className="nav__list-item">
                <NavLink exact to="/" activeClassName="is-active">Szukaj pogody</NavLink>
              </li>

              <li className="nav__list-item">
                <NavLink to="/weather-today" activeClassName="is-active">Pogoda na dziś</NavLink>
              </li>
              
              <li className="nav__list-item">
                <NavLink to="/weather-longterm" activeClassName="is-active">Pogoda długoterminowa</NavLink>
              </li>
            </ul>
          </nav>

          <div className="content-wrapper">
            <div className="bg-img">
              <img src={process.env.PUBLIC_URL + '/bg.png'} alt="logo" />
            </div>

            <Switch>
              <Route exact path="/">
                <LocationContainer />
              </Route>
  
              <Route path="/weather-today">
                <CurrentContainer />
              </Route>
  
              <Route path="/weather-longterm">
                <LongtermContainer />
              </Route>
            </Switch>
          </div>
        </HashRouter>
      </div>
    );
  }
}

export default App;

