import React, {Component} from "react";
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {fetchLongtermWeather} from '../../actions';
import types from '../../actions';
import './Longterm.scss';

class Longterm extends Component {
  setCurrenPosition() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        return [
          this.props.fetchLongtermWeather(position.coords)
        ]
      });
    } else {
      alert(types.ALERT);
    }
  }

  componentDidMount() {
    this.setCurrenPosition();
  }

  render() {
    const {longtermWeather} = this.props;

    return (
      <section className="sec">
        <h2 className="sr-only">
          Pogoda długoterminowa
        </h2>

        {longtermWeather.map((item, index) => (
          <div key={index}>
            <h3 className="sec__title">
              {item.city_name}
            </h3>

            <div className="cards">
              {item.data.map((subitem, subindex) => (
                <div className="card" key={subindex}>
                  <div className="img-wrapper">
                    <img src={`https://www.weatherbit.io/static/img/icons/${subitem.weather.icon}.png`} alt={subitem.weather.description} />
                  </div>

                  <div class="temp txt-fw-average">
                    {subitem.temp} <sup>o</sup>C
                  </div>

                  <div class="info">
                    <h3 class="txt-h4 txt-fw-average">{subitem.valid_date}</h3>
                    <p>{subitem.weather.description}</p>
                    <p>Wiatr: {subitem.wind_spd} m/s</p>
                    <p>Kierunek wiatru: {subitem.wind_cdir_full}</p>
                  </div>
                </div>
              ))}
            </div>
          </div>
        ))}
      </section>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    longtermWeather: state.longtermWeather
  };
}

const  mapDispatchToProps = (dispatch) => {
  return bindActionCreators({fetchLongtermWeather}, dispatch)
}

const LongtermContainer = connect(mapStateToProps, mapDispatchToProps)(Longterm);
export default LongtermContainer;
