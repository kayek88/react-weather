import types from '../actions';

const currentReducer = (state = [], action) => {
  switch (action.type) {
    case types.FETCH_CURRENT_WEATHER:
      return [action.payload.data[0]];
    default:
      return state;
  }
}
                            
export default currentReducer