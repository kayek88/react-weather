import types from '../actions';

const longtermReducer = (state = [], action) => {
  switch (action.type) {
    case types.FETCH_LONGTERM_WEATHER:
      return [action.payload];
    default:
      return state;
  }
}
                            
export default longtermReducer