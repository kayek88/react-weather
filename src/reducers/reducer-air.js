import types from '../actions';

const airConditionReducer = (state = [], action) => {
  switch (action.type) {
    case types.FETCH_AIR_CONDITION_WEATHER:
      return [action.payload.data[0]];
    default:
      return state;
  }
}
                            
export default airConditionReducer