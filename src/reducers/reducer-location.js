import types from '../actions';

const initialState = []

const locationReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.FETCH_LOCATION_WEATHER:
      return [
        ...state,
        action.payload.data[0]
      ];
    case types.RESET_LOCATION_WEATHER:
        return initialState;
    default:
      return state;
  }
}
                            
export default locationReducer