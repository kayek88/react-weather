import {combineReducers} from 'redux';
import locationReducer from './reducer-location';
import longtermReducer from './reducer-longterm';
import currentReducer from './reducer-current';
import airConditionReducer from './reducer-air';

const rootReducer = combineReducers({
  weather: locationReducer,
  longtermWeather: longtermReducer,
  currentWeather: currentReducer,
  airCondition: airConditionReducer
})

export default rootReducer;