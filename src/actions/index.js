import axios from 'axios'
import types from './types.js';

export const locationWeather = (data) => {
  return {
    type: types.FETCH_LOCATION_WEATHER,
    payload: data
  }
}

export const resetLocationWeather = () => {
  return {
    type: types.RESET_LOCATION_WEATHER
  }
}

export const currentWeather = (data) => {
  return {
    type: types.FETCH_CURRENT_WEATHER,
    payload: data
  }
}

export const longtermWeather = (data) => {
  return {
    type: types.FETCH_LONGTERM_WEATHER,
    payload: data
  }
}

export const airCondition = (data) => {
  return {
    type: types.FETCH_AIR_CONDITION_WEATHER,
    payload: data
  }
}


export const fetchLocationWeather = (city) => {
  const url = "https://api.weatherbit.io/v2.0/current?&lang=pl&city=" + city + types.API_KEY;

  return (dispatch) => {
    axios.get(url)
    .then(response => {
      dispatch(locationWeather(response.data));
    })
    .catch(error => {
      alert(types.NO_LOCATION);
    })
  }
}

export const fetchLongtermWeather = (position) => {
  const location = '&lat=' + position.latitude + '&lon=' + position.longitude;
  const url = "https://api.weatherbit.io/v2.0/forecast/daily?&days=16&lang=pl" + location + types.API_KEY;

  return (dispatch) => {
    axios.get(url)
    .then(response => {
      dispatch(longtermWeather(response.data));
    })
    .catch(error => {
      console.log(error);
    })
  }
}

export const fetchCurrentWeather = (position) => {
  const location = '&lat=' + position.coords.latitude + '&lon=' + position.coords.longitude;
  const url = 'https://api.weatherbit.io/v2.0/current?&lang=pl' + location + types.API_KEY;

  return (dispatch) => {
    axios.get(url)
    .then(response => {
      dispatch(currentWeather(response.data));
    })
    .catch(error => {
      console.log(error);
    })
  }
}

export const fetchAirCondition = (position) => {
  const location = '&lat=' + position.coords.latitude + '&lon=' + position.coords.longitude;
  const url = 'https://api.weatherbit.io/v2.0/current/airquality?&lang=pl' + location + types.API_KEY;

  return (dispatch) => {
    axios.get(url)
    .then(response => {
      dispatch(airCondition(response.data));
    })
    .catch(error => {
      console.log(error);
    })
  }
}

export default types;